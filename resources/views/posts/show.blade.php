@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-text text-muted">Author: {{$post->user->name}}</p>
			<p class="card-text text-muted">Likes: {{count($post->likes)}}</p>
			<p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>
			{{-- Check if the user is logged in --}}
			@if(Auth::user())
				{{-- Check if the post author is NOT the current user --}}
				@if(Auth::user()->id != $post->user_id)
					<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
						@method('PUT')
						@csrf
						@if($post->likes->contains("user_id", Auth::user()->id))
							<button type="submit" class="btn btn-danger">Unlike</button>
						@else
							<button type="submit" class="btn btn-success">Like</button>
						@endif
					</form>
				@endif
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Comment</button>
			@endif

			<div class="mt-3">
				<a href="/posts" class="card-link">View all posts</a>
			</div>
		</div>
		<div class="card-footer">
			@if(count($post->comments) > 0)
				@foreach($post->comments as $comment)
					<p>{{$comment}}</p>
				@endforeach
			@else
				<p>There are no comments in this post.</p>
			@endif
		</div>
	</div>
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">New Comment</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form method="POST" action="/posts/{{$post->id}}/comment">
					@csrf
					<div class="modal-body">
						<div class="form-group">
							<label for="content" class="col-form-label">Content:</label>
							<textarea class="form-control" id="content" name="content"></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Post Comment</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection